import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { UrlService } from '../services/url.service'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NguCarouselModule } from '@ngu/carousel';


//Angular material
import { MatIconModule } from '@angular/material/icon';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { MatIconRegistry } from '@angular/material/icon';
import { InteriorsComponent } from './interiors/interiors.component';
import { PlansComponent } from './plans/plans.component';
import { PurchaseOptionsComponent } from './purchase-options/purchase-options.component';
import { ContactComponent } from './contact/contact.component';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { BookingComponent } from '../shared/booking/booking.component';
import { MatMenuModule } from '@angular/material/menu';
import { MenuComponent } from '../shared/menu/menu.component';
import { MatDialogModule, MatDialogRef} from '@angular/material/dialog';



@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    HomeComponent,
    InteriorsComponent,
    PlansComponent,
    PurchaseOptionsComponent,
    ContactComponent,
    NavbarComponent,
    BookingComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    MatCarouselModule.forRoot(),
    AppRoutingModule,
    MatInputModule,
    NguCarouselModule,
    MatButtonModule,
    MatMenuModule,
    MatDialogModule,
    MatIconModule,
    MatGridListModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule
  ],
  providers: [MatIconRegistry, UrlService, {provide: MatDialogRef, useValue: {}}],
  bootstrap: [AppComponent]
})
export class AppModule { };
