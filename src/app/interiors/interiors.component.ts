import { Component, OnInit, ViewChild, Inject,HostListener } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatCarousel, MatCarouselComponent, MatCarouselSlideComponent } from '@ngmodule/material-carousel';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-interiors',
  templateUrl: './interiors.component.html',
  styleUrls: ['./interiors.component.scss']
})
export class InteriorsComponent implements OnInit {
  @ViewChild(MatCarouselComponent) carousel: MatCarouselComponent;
  // @ViewChild(MatCarouselSlideComponent) slide: MatCarouselSlideComponent;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = event.target.outerWidth
    if(this.screenWidth <= 900){
      this.proportion = 70;
    }else{
      this.proportion = 50;
    }
  }
  slides = [
    // '../../assets/image/interiors/1.png',
    // '../../assets/image/interiors/2.png',
    // '../../assets/image/interiors/3.png',
    '../../assets/image/interiors/Livingroom-Sizes.png',
    '../../assets/image/interiors/Bedroom.png',
  ]
  screenWidth:any;
  proportion=50
  slideName :any
  slideNames = [
    "Open living and dining area",
    "Master bedroom with w/c",
    // "Kitchen",
  ]
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, @Inject(DOCUMENT) private document: Document) {
    iconRegistry.addSvgIcon(
      'gridLines',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/GRID LINES ICON.svg'));
    iconRegistry.addSvgIcon(
      'mail',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mail.svg'));
    iconRegistry.addSvgIcon('arrowBack', sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/left_caroussel_white.svg'));
    iconRegistry.addSvgIcon('arrowForward', sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/right_caroussel_white.svg'));
  }

  ngOnInit(): void {
    this.slideName = this.slideNames[0];
  }

  ngAfterViewInit() {

   setTimeout(() => {
    this.screenWidth = window.innerWidth;
    if(this.screenWidth <= 900){
      this.proportion = 70;
     }

      this.carousel.svgIconOverrides = { arrowBack: 'arrowBack', arrowForward: 'arrowForward' }


    // this.carousel.maintainAspectRatio = false

    let d = new DOMParser().parseFromString(`<div class="slideNameDiv">
    <p class="slideName" style=" text-align: center;
    color: #2d343c;
    font-weight: bold;
    transition: 0.2s;">${this.slideNames[0]}</p>
</div> <style>
.slideNameDiv {
  padding-top: 5px;
  padding-bottom: 4px;
  background-color: #fff;
  width: 35%;
  position: absolute;
  margin: 0 auto;
  left: 32.5%;
  // right: -10%
  margin-bottom: 20px;
  bottom: 2%;
}

@media only screen and (max-width: 1280px) {
  .slideNameDiv {
    left: 10%;
    width: 80%;
    margin: 0 auto;
  }
}</style>`, "text/html");
    this.document.querySelector(".carousel").insertAdjacentHTML('beforeend', d.body.innerHTML)

    this.carousel.change.subscribe((e) => {
      this.slideName = this.slideNames[e]

      let d = new DOMParser().parseFromString(`<p class="slideName" style=" text-align: center;color: #2d343c;font-weight:bold;transition: 0.2s;">${this.slideName}</p>`, "text/html")

      this.document.querySelector(".slideName").replaceWith(d.body.querySelector(".slideName"))
    })
   }, 1);

  }
}
