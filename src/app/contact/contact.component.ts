import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class ContactComponent implements OnInit {

  constructor(iconRegistry: MatIconRegistry, public sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('mail',sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mail.svg'));

  }

  ngOnInit(): void {
  }

}
