import { Component, Inject, OnInit, ViewChild, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatCarousel, MatCarouselComponent, MatCarouselSlideComponent } from '@ngmodule/material-carousel';



@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss'],
  // encapsulation: ViewEncapsulation.None

})

export class PlansComponent implements OnInit {
  @ViewChild(MatCarouselComponent) carousel: MatCarouselComponent;
  @ViewChild(MatCarouselSlideComponent) slide: MatCarouselSlideComponent;

  slides = [
    '../../assets/image/plans/Ground floor plan-semi-detached unit.png',
    '../../assets/image/plans/block plan.png'
  ]
  slideName = ""
  details = ""

  slideInfo = [
    { name: 'Ground floor plan-semi-detached unit', details: "The Ground Floor - Upon entering the home on the ground floor, you are greeted by a welcoming reception area which ties together the living and dining areas which are on its flanks.  The dining area in turn leads into a beautiful and contemporary kitchen with a breakfast nook.  Off to the side of the dining area is a corridor that leads to the powder room and a guest suite.  The sliding glass doors of the living area lead off to the lawn on the side of the building and the pool terrace in the rear.  The ground floor's functionality is rounded off with a scullery (outside kitchen) and staff quarters which are accessed through a doorway next to the stairs that lead to the middle and upper floors." },
    {
      name: "Block plan layout", details: `The External Grounds - The grounds of each house feature a generous garden, an optional swimming pool, and parking for 3 cars.  For added privacy and security, each house comes with an individual gate, a CCTV system, a backup generator and an ample water reservoir.
      `}
  ]
  constructor(iconRegistry: MatIconRegistry, public sanitizer: DomSanitizer, private cdr: ChangeDetectorRef) {
    iconRegistry.addSvgIcon('gridLines', sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/GRID LINES ICON.svg'));
    iconRegistry.addSvgIcon('mail', sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mail.svg'));
    iconRegistry.addSvgIcon('arrowBack', sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/left_caroussel.svg'));
    iconRegistry.addSvgIcon('arrowForward', sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/right_caroussel.svg'));

  }

  ngOnInit(): void {
    this.slideName = this.slideInfo[0].name
    this.details = this.slideInfo[0].details
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.carousel.svgIconOverrides = { arrowBack: 'arrowBack', arrowForward: 'arrowForward' }
      this.carousel.change.subscribe((e) => {
        this.slideName = this.slideInfo[e].name
        this.details = this.slideInfo[e].details
      })
    }, 100);
    // console.log(this.carousel);

  }

}
