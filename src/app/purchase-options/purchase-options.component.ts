import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
@Component({
  selector: 'app-purchase-options',
  templateUrl: './purchase-options.component.html',
  styleUrls: ['./purchase-options.component.scss']
})
export class PurchaseOptionsComponent implements OnInit {

  constructor(iconRegistry: MatIconRegistry, public sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('gridLines',sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/GRID LINES ICON.svg'));
    iconRegistry.addSvgIcon('mail',sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mail.svg'));

    iconRegistry.addSvgIcon('date',sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/date.svg'));
   }

  ngOnInit(): void {
  }

}
