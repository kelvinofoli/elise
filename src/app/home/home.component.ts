import { Component, OnInit, ViewChild, Inject,Compiler } from '@angular/core';
import { MatCarousel, MatCarouselComponent } from '@ngmodule/material-carousel';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';

import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  slides: any
  carouselHgt: any
  @ViewChild(MatCarouselComponent) carousel: MatCarouselComponent;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,private compiler:Compiler, @Inject(DOCUMENT) private document: Document) {
    this.slides = [
      '../../assets/image/1.png',
      '../../assets/image/Garden.png',
      '../../assets/image/Pool.png',
      '../../assets/image/Terrace-Day.png',
      '../../assets/image/Terrace-night.png',
    ]
    iconRegistry.addSvgIcon(
      'gridLines',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/GRID LINES ICON.svg'));
    iconRegistry.addSvgIcon(
      'mail',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mail.svg'));
    iconRegistry.addSvgIcon(
      'left',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/left-arrow.svg'));
    iconRegistry.addSvgIcon(
      'right',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/right-arrow.svg'));
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      // var divOffset = this.offset(document.querySelector('._carousel'));
      // var _divOffset = this.offset(document.querySelector('.yeppp'));
      // let  b = document.querySelector('._carousel').getBoundingClientRect().top
      // let a = document.querySelector('.yeppp').getBoundingClientRect().top
      // this.carouselHgt =
      // console.log(this.carouselHgt);

      // let s = document.querySelector('.yeppp')
      // s.style.bottom = `${this.carouselHgt}px`
    }, 1);
    let d = this.document.querySelector('.yeppp')
    // this.compiler.compileModuleAndAllComponentsSync(d.boby)
    // this.document.querySelector(".carousel").insertAdjacentHTML('beforeend', d)
  }

  nextslide() {
    this.carousel.next()
  }
  prevslide() {
    this.carousel.previous()
  }
}
