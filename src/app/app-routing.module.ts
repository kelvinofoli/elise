import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { InteriorsComponent } from './interiors/interiors.component';
import { PlansComponent } from './plans/plans.component';
import { PurchaseOptionsComponent } from './purchase-options/purchase-options.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  {path:'',component: WelcomeComponent},
  {path:'home',component: HomeComponent},
  {path:'interiors',component: InteriorsComponent},
  {path:'plans',component: PlansComponent},
  {path:'purchase',component: PurchaseOptionsComponent},
  {path:'contact',component: ContactComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash:true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
