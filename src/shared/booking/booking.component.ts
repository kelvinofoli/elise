import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
  element :any
  myStorage :any
  contact = false

  constructor(private router: Router, private route: ActivatedRoute,) {
    if (this.router.url == "/contact") {
      this.contact = true
    } else {
      this.contact = false

    }

  }

  ngOnInit(): void {
    this.element = $('.floating-chat');
    this.myStorage = localStorage;
    // if (!this.myStorage.getItem('chatID')) {
    //   this.myStorage.setItem('chatID', this.createUUID());
    // }
    // setTimeout(function () {
    //   this.element.addClass('enter');
    // }, 1000);

    // this.element.click(this.openElement);
  }

  openElement() {
    var messages = this.element.find('.messages');
    var textInput = this.element.find('.text-box');
    this.element.find('>i').hide();
    this.element.addClass('expand');
    this.element.find('.chat').addClass('enter');
    var strLength = textInput.val().length * 2;
    textInput.keydown(this.onMetaAndEnter).prop("disabled", false).focus();
    this.element.off('click', this.openElement);
    this.element.find('.header button').click(this.closeElement);
    this.element.find('#sendMessage').click(this.sendNewMessage);
    messages.scrollTop(messages.prop("scrollHeight"));
  }

  closeElement() {
    this.element.find('.chat').removeClass('enter').hide();
    this.element.find('>i').show();
    this.element.removeClass('expand');
    this.element.find('.header button').off('click', this.closeElement);
    this.element.find('#sendMessage').off('click', this.sendNewMessage);
    this.element.find('.text-box').off('keydown', this.onMetaAndEnter).prop("disabled", true).blur();
    setTimeout(function () {
      this.element.find('.chat').removeClass('enter').show()
      this.element.click(this.openElement);
    }, 500);
  }


  createUUID() {
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
  }


  sendNewMessage() {
    var userInput = $('.text-box');
    var newMessage = userInput.html().replace(/\<div\>|\<br.*?\>/ig, '\n').replace(/\<\/div\>/g, '').trim().replace(/\n/g, '<br>');

    if (!newMessage) return;

    var messagesContainer = $('.messages');

    messagesContainer.append([
      '<li class="self">',
      newMessage,
      '</li>'
    ].join(''));

    // clean out old message
    userInput.html('');
    // focus on input
    userInput.focus();

    messagesContainer.finish().animate({
      scrollTop: messagesContainer.prop("scrollHeight")
    }, 250);
  }

  onMetaAndEnter(event) {
    if ((event.metaKey || event.ctrlKey) && event.keyCode == 13) {
      this.sendNewMessage();
    }
  }


  async sendEmail() {
    // // Generate test SMTP service account from ethereal.email
    // // Only needed if you don't have a real mail account for testing
    // let testAccount = await nodemailer.createTestAccount();

    // // create reusable transporter object using the default SMTP transport
    // let transporter = nodemailer.createTransport({
    //   host: "smtp.ethereal.email",
    //   port: 587,
    //   secure: false, // true for 465, false for other ports
    //   auth: {
    //     user: testAccount.user, // generated ethereal user
    //     pass: testAccount.pass, // generated ethereal password
    //   },
    // });

    // // send mail with defined transport object
    // let info = await transporter.sendMail({
    //   from: '"Fred Foo 👻" <foo@example.com>', // sender address
    //   to: "ofolikelvin@gmail.com", // list of receivers
    //   subject: "Hello ✔", // Subject line
    //   text: "Hello world?", // plain text body
    //   html: "<b>Hello world?</b>", // html body
    // });

    // console.log("Message sent: %s", info.messageId);
    // // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // // Preview only available when sending through an Ethereal account
    // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  }


}
