import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MenuComponent} from '../menu/menu.component'
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  url: any
  constructor(public router: Router,public dialog: MatDialog) {
    this.url =  this.router.url
  }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MenuComponent, {
      // position: {
      //   top: '10px',
      //   right: '10px'
      // },
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '100%',
      width: '100%',
      panelClass: 'full-screen-modal',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  // _click() {
    // sessionStorage.setItem('from', this.url);
    // this.router.navigate([`/menu`])
  // }

}
