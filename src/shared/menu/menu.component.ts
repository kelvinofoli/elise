import { Component, OnInit, Inject } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  url: string
  constructor(
    public dialog: MatDialog,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    public router: Router,
    public dialogRef: MatDialogRef<MenuComponent>,
    public route: ActivatedRoute) {
    iconRegistry.addSvgIcon('mail', sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/mail.svg'));

  }

  ngOnInit(): void {
    this.url = this.router.url

  }

  close() {
    // this._location.back();
    this.dialogRef.close();
  }

  goTo(loc) {
    this.router.navigate([loc])
    this.dialogRef.close();
  }

}
